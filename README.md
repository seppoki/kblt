# kblt

This tool generates optimised keyboard layouts for a given corpus.

## Usage

Build the executable

    zig build-exe -O ReleaseFast src/main.zig

Run the executable with a given corpus as the only parameter

    ./main corpus/word-pairs-stripped

Note that the corpus is to be structured such that each line is prefixed with an integer which is used to weight that particular line in relation to the other lines in said corpus. Check the correct format from the sample english corpus *corpus/word-pairs-stripped*.

The project also has an evaluation program which runs no optimization but instead evaluates every layout in the *layouts* folder.

Build this executable with

    zig build-exe -O ReleaseFast srd/eval.zig

and run it with

    ./eval corpus/word-pairs-stripped

## configuration

The to be optimised character set can be defined in the *src/model.zig* file.

Optimization parameters can be modified in the *src/evaluation.zig* file.
