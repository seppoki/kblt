const std = @import("std");

pub const Monogram = struct {
    index: usize,
    freq: f32,
};

pub const Bigram = struct {
    first_index: usize,
    second_index: usize,
    freq: f32,
};

pub const Trigram = struct {
    first_index: usize,
    second_index: usize,
    third_index: usize,
    freq: f32,
};

pub const Model = struct {
    symbols: []u21,
    monograms: []Monogram,
    bigrams: []Bigram,
    skipgrams: []Bigram,
    trigrams: []Trigram,
};

pub fn getModel(path: []const u8, allocator: std.mem.Allocator) !*Model {
    var file = try std.fs.cwd().openFile(path, .{ .mode = std.fs.File.OpenMode.read_only });
    defer file.close();
    var buf_reader = std.io.bufferedReader(file.reader());
    var file_reader = buf_reader.reader();

    var monogram_map = std.AutoArrayHashMap(u21, u64).init(allocator);
    defer monogram_map.clearAndFree();

    var bigram_map = std.AutoArrayHashMap([2]u21, u64).init(allocator);
    defer bigram_map.clearAndFree();

    var skipgram_map = std.AutoArrayHashMap([2]u21, u64).init(allocator);
    defer skipgram_map.clearAndFree();

    var trigram_map = std.AutoArrayHashMap([3]u21, u64).init(allocator);
    defer trigram_map.clearAndFree();

    const sym_ar = [_]u8{ ' ', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y' };
    var symbols = try allocator.alloc(u21, sym_ar.len);
    errdefer allocator.free(symbols);
    for (sym_ar) |s, i| {
        symbols[i] = try std.unicode.utf8Decode(&[1]u8{s});
    }

    var monogram_count: u64 = 0;
    var bigram_count: u64 = 0;
    var skipgram_count: u64 = 0;
    var trigram_count: u64 = 0;
    var buffer: [1024 * 10]u8 = undefined;
    while (try file_reader.readUntilDelimiterOrEof(&buffer, '\n')) |line| {
        var tokens = std.mem.tokenize(u8, line, " ");
        const count_str = tokens.peek().?;
        const count = try std.fmt.parseInt(u64, count_str, 10);
        const rest = std.mem.trimLeft(u8, line, "0123456789");

        var utf8 = (try std.unicode.Utf8View.init(rest)).iterator();

        while (utf8.nextCodepoint()) |cp| {
            _ = std.mem.indexOfScalar(u21, symbols, cp) orelse continue;

            monogram_count += count;
            if (!monogram_map.contains(cp)) {
                try monogram_map.put(cp, count);
            } else {
                monogram_map.getPtr(cp).?.* += count;
            }
        }

        utf8 = (try std.unicode.Utf8View.init(rest)).iterator();
        var pre = utf8.nextCodepoint() orelse continue;
        while (utf8.nextCodepoint()) |cp| {
            const bg: [2]u21 = [2]u21{ pre, cp };
            pre = cp;

            _ = std.mem.indexOfScalar(u21, symbols, bg[0]) orelse continue;
            _ = std.mem.indexOfScalar(u21, symbols, bg[1]) orelse continue;

            bigram_count += count;
            if (!bigram_map.contains(bg)) {
                try bigram_map.put(bg, count);
            } else {
                bigram_map.getPtr(bg).?.* += count;
            }
        }

        utf8 = (try std.unicode.Utf8View.init(rest)).iterator();
        var pres2 = utf8.nextCodepoint() orelse continue;
        var pres1 = utf8.nextCodepoint() orelse continue;
        while (utf8.nextCodepoint()) |cp| {
            const sg: [2]u21 = [2]u21{ pres2, cp };
            pres2 = pres1;
            pres1 = cp;

            _ = std.mem.indexOfScalar(u21, symbols, sg[0]) orelse continue;
            _ = std.mem.indexOfScalar(u21, symbols, sg[1]) orelse continue;

            skipgram_count += count;
            if (!skipgram_map.contains(sg)) {
                try skipgram_map.put(sg, count);
            } else {
                skipgram_map.getPtr(sg).?.* += count;
            }
        }

        utf8 = (try std.unicode.Utf8View.init(rest)).iterator();
        var pre2 = utf8.nextCodepoint() orelse continue;
        var pre1 = utf8.nextCodepoint() orelse continue;
        while (utf8.nextCodepoint()) |cp| {
            const tg: [3]u21 = [3]u21{ pre2, pre1, cp };
            pre2 = pre1;
            pre1 = cp;

            _ = std.mem.indexOfScalar(u21, symbols, tg[0]) orelse continue;
            _ = std.mem.indexOfScalar(u21, symbols, tg[1]) orelse continue;
            _ = std.mem.indexOfScalar(u21, symbols, tg[2]) orelse continue;

            trigram_count += count;
            if (!trigram_map.contains(tg)) {
                try trigram_map.put(tg, count);
            } else {
                trigram_map.getPtr(tg).?.* += count;
            }
        }
    }
    monogram_map.sort(sorter{ .values = monogram_map.values() });
    var monograms = try allocator.alloc(Monogram, symbols.len);
    errdefer allocator.free(monograms);

    var monogram_index: usize = 0;
    var monogram_iter = monogram_map.iterator();
    while (monogram_iter.next()) |kv| {
        var freq: f32 = @intToFloat(f32, kv.value_ptr.*) / @intToFloat(f32, monogram_count);

        const index = std.mem.indexOfScalar(u21, symbols, kv.key_ptr.*).?;
        monograms[monogram_index] = Monogram{ .index = index, .freq = freq };
        monogram_index += 1;
    }

    bigram_map.sort(sorter{ .values = bigram_map.values() });
    var bigrams = try allocator.alloc(Bigram, bigram_map.count());
    errdefer allocator.free(bigrams);

    var bigram_index: usize = 0;
    var freq_accu: f32 = 0.0;
    const bigram_freq_limit: f32 = 0.995;
    var bigram_iter = bigram_map.iterator();
    while (bigram_iter.next()) |kv| {
        var freq: f32 = @intToFloat(f32, kv.value_ptr.*) / @intToFloat(f32, bigram_count);
        freq_accu += freq;
        const first_index = std.mem.indexOfScalar(u21, symbols, kv.key_ptr.*[0]) orelse continue;
        const second_index = std.mem.indexOfScalar(u21, symbols, kv.key_ptr.*[1]) orelse continue;
        bigrams[bigram_index] = Bigram{ .first_index = first_index, .second_index = second_index, .freq = freq / bigram_freq_limit };
        bigram_index += 1;
        if (freq_accu > bigram_freq_limit) break;
    }

    skipgram_map.sort(sorter{ .values = skipgram_map.values() });
    var skipgrams = try allocator.alloc(Bigram, skipgram_map.count());
    errdefer allocator.free(skipgrams);

    var skipgram_index: usize = 0;
    freq_accu = 0.0;
    const skipgram_freq_limit: f32 = 0.995;
    var skipgram_iter = skipgram_map.iterator();
    while (skipgram_iter.next()) |kv| {
        var freq: f32 = @intToFloat(f32, kv.value_ptr.*) / @intToFloat(f32, skipgram_count);
        freq_accu += freq;
        const first_index = std.mem.indexOfScalar(u21, symbols, kv.key_ptr.*[0]) orelse continue;
        const second_index = std.mem.indexOfScalar(u21, symbols, kv.key_ptr.*[1]) orelse continue;
        skipgrams[skipgram_index] = Bigram{ .first_index = first_index, .second_index = second_index, .freq = freq / skipgram_freq_limit };
        skipgram_index += 1;
        if (freq_accu > skipgram_freq_limit) break;
    }

    trigram_map.sort(sorter{ .values = trigram_map.values() });
    var trigrams = try allocator.alloc(Trigram, trigram_map.count());
    errdefer allocator.free(trigrams);

    var trigram_index: usize = 0;
    freq_accu = 0.0;
    const trigram_freq_limit: f32 = 0.995;
    var trigram_iter = trigram_map.iterator();
    while (trigram_iter.next()) |kv| {
        var freq: f32 = @intToFloat(f32, kv.value_ptr.*) / @intToFloat(f32, trigram_count);
        freq_accu += freq;
        const first_index = std.mem.indexOfScalar(u21, symbols, kv.key_ptr.*[0]) orelse continue;
        const second_index = std.mem.indexOfScalar(u21, symbols, kv.key_ptr.*[1]) orelse continue;
        const third_index = std.mem.indexOfScalar(u21, symbols, kv.key_ptr.*[2]) orelse continue;
        trigrams[trigram_index] = Trigram{ .first_index = first_index, .second_index = second_index, .third_index = third_index, .freq = freq / trigram_freq_limit };
        trigram_index += 1;
        if (freq_accu > trigram_freq_limit) break;
    }

    var model = try allocator.create(Model);
    model.symbols = symbols;

    model.monograms = try allocator.alloc(Monogram, symbols.len);
    std.mem.copy(Monogram, model.monograms, monograms);
    allocator.free(monograms);

    model.bigrams = try allocator.alloc(Bigram, bigram_index);
    std.mem.copy(Bigram, model.bigrams, bigrams[0..bigram_index]);
    allocator.free(bigrams);

    model.skipgrams = try allocator.alloc(Bigram, skipgram_index);
    std.mem.copy(Bigram, model.skipgrams, skipgrams[0..skipgram_index]);
    allocator.free(skipgrams);

    model.trigrams = try allocator.alloc(Trigram, trigram_index);
    std.mem.copy(Trigram, model.trigrams, trigrams[0..trigram_index]);
    allocator.free(trigrams);

    return model;
}

const sorter = struct {
    values: []u64,
    pub fn lessThan(ctx: @This(), a: usize, b: usize) bool {
        return ctx.values[a] > ctx.values[b];
    }
};
