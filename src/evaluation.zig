const std = @import("std");
const Analysis = @import("analysis.zig").Analysis;

pub fn cost(val: f32, limit: f32, range: f32) f32 {
    return if (val <= limit) 0.0 else ((val - limit) / range) * ((val - limit) / range);
}

pub fn evaluateMono(a: Analysis) f32 {
    var u: f32 = 0.0;

    u += cost(0.592104 - (a.l.effort[0] + a.r.effort[0]), 0.0, 0.015);
    u += cost(0.888288 - (a.l.effort[0] + a.r.effort[0] + a.l.effort[1] + a.r.effort[1]), 0.0, 0.015);
    u += cost(0.97029 - (a.l.effort[0] + a.r.effort[0] + a.l.effort[1] + a.r.effort[1] + a.l.effort[2] + a.r.effort[2]), 0.0, 0.015);

    u += cost(a.l.effort[4], 0.0, 0.004);
    u += cost(a.r.effort[4], 0.0, 0.004);
    u += cost(a.l.effort[5], 0.0, 0.004);
    u += cost(a.r.effort[5], 0.0, 0.004);

    u += cost(a.l.p, 0.08, 0.005);
    u += cost(a.r.p, 0.08, 0.005);

    u += cost(a.l.is, 0.0, 0.015);
    u += cost(a.r.is, 0.0, 0.015);
    return u;
}

pub fn evaluateBigram(a: Analysis) f32 {
    // _ = a;
    var u: f32 = 0.0;

    u += cost(a.l.great_sfb, 0.0, 0.001);
    u += cost(a.r.great_sfb, 0.0, 0.001);
    u += cost(a.l.good_sfb, 0.0, 0.0002);
    u += cost(a.r.good_sfb, 0.0, 0.0002);
    u += cost(a.l.average_sfb, 0.0, 0.0001);
    u += cost(a.r.average_sfb, 0.0, 0.0001);
    u += cost(a.l.bad_sfb, 0.0, 0.00005);
    u += cost(a.r.bad_sfb, 0.0, 0.00005);

    u += cost(a.l.pair[6], 0.0, 0.00015);
    u += cost(a.r.pair[6], 0.0, 0.00015);
    u += cost(a.l.pair[5], 0.0, 0.001);
    u += cost(a.r.pair[5], 0.0, 0.001);
    u += cost(a.l.pair[4], 0.0, 0.003);
    u += cost(a.r.pair[4], 0.0, 0.003);
    u += cost(a.l.pair[3], 0.0, 0.01);
    u += cost(a.r.pair[3], 0.0, 0.01);
    u += a.l.pair[6] * 120;
    u += a.r.pair[6] * 120;
    u += a.l.pair[5] * 100;
    u += a.r.pair[5] * 100;
    u += a.l.pair[4] * 80;
    u += a.r.pair[4] * 80;
    u += a.l.pair[3] * 60;
    u += a.r.pair[3] * 60;
    u += a.l.pair[2] * 40;
    u += a.r.pair[2] * 40;
    u += a.l.pair[1] * 20;
    u += a.r.pair[1] * 20;

    for (a.l.roll) |r, i| {
        u += r * @intToFloat(f32, i) * 0.5;
    }
    for (a.r.roll) |r, i| {
        u += r * @intToFloat(f32, i) * 0.5;
    }
    u += cost(a.l.roll[16], 0.015, 0.01);
    u += cost(a.r.roll[16], 0.015, 0.01);
    u += cost(a.l.roll[17], 0.015, 0.01);
    u += cost(a.r.roll[17], 0.015, 0.01);
    u += cost(a.l.roll[18], 0.015, 0.01);
    u += cost(a.r.roll[18], 0.015, 0.01);
    u += cost(a.l.roll[19], 0.015, 0.01);
    u += cost(a.r.roll[19], 0.015, 0.01);

    u += cost(a.l.pinky_repeat, 0.001, 0.002);
    u += cost(a.r.pinky_repeat, 0.001, 0.002);

    // u += cost(0.20 - (a.l.roll[0] + a.r.roll[0] + a.l.roll[1] + a.r.roll[1] + a.l.roll[4] + a.r.roll[4] + a.l.roll[5] + a.r.roll[5]), 0.0, 0.01);
    // u += cost(0.05 - a.r.roll[0], 0.0, 0.01);
    // u += cost(0.05 - a.l.roll[1], 0.0, 0.01);
    // u += cost(0.05 - a.r.roll[1], 0.0, 0.01);
    // u += cost(0.05 - a.l.roll[4], 0.0, 0.01);
    // u += cost(0.05 - a.r.roll[4], 0.0, 0.01);
    // u += cost(0.05 - a.l.roll[5], 0.0, 0.01);
    // u += cost(0.05 - a.r.roll[5], 0.0, 0.01);

    return u;
}

pub fn evaluateSkipgram(a: Analysis) f32 {
    // _ = a;
    var u: f32 = 0.0;

    // u += cost(a.l.great_sfs, 0.01, 0.005);
    // u += cost(a.r.great_sfs, 0.01, 0.005);
    // u += cost(a.l.good_sfs, 0.01, 0.005);
    // u += cost(a.r.good_sfs, 0.01, 0.005);
    // u += cost(a.l.average_sfs, 0.005, 0.0025);
    // u += cost(a.r.average_sfs, 0.005, 0.0025);
    // u += cost(a.l.bad_sfs, 0.001, 0.0005);
    // u += cost(a.r.bad_sfs, 0.001, 0.0005);

    u += cost(a.l.spair[6], 0.0, 0.0005);
    u += cost(a.r.spair[6], 0.0, 0.0005);
    u += cost(a.l.spair[5], 0.0, 0.001);
    u += cost(a.r.spair[5], 0.0, 0.001);
    u += cost(a.l.spair[4], 0.0, 0.01);
    u += cost(a.r.spair[4], 0.0, 0.01);
    u += cost(a.l.spair[3], 0.0, 0.02);
    u += cost(a.r.spair[3], 0.0, 0.02);
    u += a.l.spair[6] * 12;
    u += a.r.spair[6] * 12;
    u += a.l.spair[5] * 10;
    u += a.r.spair[5] * 10;
    u += a.l.spair[4] * 8;
    u += a.r.spair[4] * 8;
    u += a.l.spair[3] * 6;
    u += a.r.spair[3] * 6;
    u += a.l.spair[2] * 4;
    u += a.r.spair[2] * 4;
    u += a.l.spair[1] * 2;
    u += a.r.spair[1] * 2;

    for (a.l.sroll) |r, i| {
        u += r * @intToFloat(f32, i) * 0.05;
    }
    for (a.r.sroll) |r, i| {
        u += r * @intToFloat(f32, i) * 0.05;
    }

    u += cost(a.l.pinky_srepeat, 0.001, 0.003);
    u += cost(a.r.pinky_srepeat, 0.001, 0.003);

    return u;
}

pub fn evaluateTrigram(a: Analysis) f32 {
    // _ = a;
    var u: f32 = 0.0;

    u += cost(0.70 - (a.l.ro + a.r.ro), 0.0, 0.015);
    // u += cost(0.35 - (a.r.ro), 0.0, 0.03);
    // u += cost(a.l.oh + a.r.oh, 0.0, 0.05);
    // u += cost(a.r.oh, 0.0, 0.03);
    // u += cost(a.l.alt + a.r.alt, 0.0, 0.05);
    // u += cost(a.r.alt, 0.0, 0.05);

    u += cost(a.l.non_sh_position, 0.0, 0.003);
    u += cost(a.r.non_sh_position, 0.0, 0.003);

    // u += cost(a.l.great_sfs_sh, 0.0, 0.003);
    // u += cost(a.r.great_sfs_sh, 0.0, 0.003);
    u += cost(a.l.good_sfs_sh, 0.0, 0.001);
    u += cost(a.r.good_sfs_sh, 0.0, 0.001);
    u += cost(a.l.average_sfs_sh, 0.0, 0.0002);
    u += cost(a.r.average_sfs_sh, 0.0, 0.0002);
    u += cost(a.l.bad_sfs_sh, 0.0, 0.00005);
    u += cost(a.r.bad_sfs_sh, 0.0, 0.00005);

    // u += cost(a.l.great_sfs_alt, 0.0, 0.010);
    // u += cost(a.r.great_sfs_alt, 0.0, 0.010);
    u += cost(a.l.good_sfs_alt, 0.0, 0.005);
    u += cost(a.r.good_sfs_alt, 0.0, 0.005);
    u += cost(a.l.average_sfs_alt, 0.0, 0.003);
    u += cost(a.r.average_sfs_alt, 0.0, 0.003);
    u += cost(a.l.bad_sfs_alt, 0.0, 0.001);
    u += cost(a.r.bad_sfs_alt, 0.0, 0.001);

    for (a.l.redir[12..]) |r, i| {
        u += r * @intToFloat(f32, i) * 1.0;
    }
    for (a.r.redir[12..]) |r, i| {
        u += r * @intToFloat(f32, i) * 1.0;
    }

    u += cost(a.l.redir[27], 0.0, 0.002);
    u += cost(a.r.redir[27], 0.0, 0.002);
    u += cost(a.l.redir[28], 0.0, 0.002);
    u += cost(a.r.redir[28], 0.0, 0.002);
    u += cost(a.l.redir[29], 0.0, 0.002);
    u += cost(a.r.redir[29], 0.0, 0.002);
    u += cost(a.l.redir[30], 0.0, 0.002);
    u += cost(a.r.redir[30], 0.0, 0.002);

    u += cost(a.l.redir[31], 0.0, 0.0015);
    u += cost(a.r.redir[31], 0.0, 0.0015);

    u += cost(a.l.redir[32], 0.0, 0.001);
    u += cost(a.r.redir[32], 0.0, 0.001);
    u += cost(a.l.redir[33], 0.0, 0.001);
    u += cost(a.r.redir[33], 0.0, 0.001);
    u += cost(a.l.redir[34], 0.0, 0.001);
    u += cost(a.r.redir[34], 0.0, 0.001);
    u += cost(a.l.redir[35], 0.0, 0.001);
    u += cost(a.r.redir[35], 0.0, 0.001);
    u += cost(a.l.redir[36], 0.0, 0.001);
    u += cost(a.r.redir[36], 0.0, 0.001);

    u += cost(a.l.redir[37], 0.0, 0.0005);
    u += cost(a.r.redir[37], 0.0, 0.0005);
    u += cost(a.l.redir[38], 0.0, 0.0005);
    u += cost(a.r.redir[38], 0.0, 0.0005);
    u += cost(a.l.redir[39], 0.0, 0.0005);
    u += cost(a.r.redir[39], 0.0, 0.0005);

    for (a.l.shRoll[5..]) |r, i| {
        u += r * @intToFloat(f32, i) * 2.0;
    }
    for (a.r.shRoll[5..]) |r, i| {
        u += r * @intToFloat(f32, i) * 2.0;
    }

    u += cost(a.l.shRoll[15], 0.0, 0.001);
    u += cost(a.r.shRoll[15], 0.0, 0.001);
    u += cost(a.l.shRoll[16], 0.0, 0.001);
    u += cost(a.r.shRoll[16], 0.0, 0.001);
    u += cost(a.l.shRoll[17], 0.0, 0.001);
    u += cost(a.r.shRoll[17], 0.0, 0.001);
    u += cost(a.l.shRoll[18], 0.0, 0.001);
    u += cost(a.r.shRoll[18], 0.0, 0.001);
    u += cost(a.l.shRoll[19], 0.0, 0.001);
    u += cost(a.r.shRoll[19], 0.0, 0.001);

    return u;
}
