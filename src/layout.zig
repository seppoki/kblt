const std = @import("std");
const bitmap = @import("bitmap.zig");
const Bitmap = bitmap.Bitmap;

var prng = std.rand.DefaultPrng.init(0);
const random = prng.random();

pub const Swap = struct {
    f: usize,
    s: usize,
};

pub fn indexOf(key: usize, layout: *[31]Bitmap) ?usize {
    const k: Bitmap = bitmap.indexToBitmap(key);
    for (layout) |l, ind| {
        if (l == k) return ind;
    }
    return null;
}

pub fn generateSwaps() [435]Swap {
    var swaps = [1]Swap{undefined} ** 435;
    var swap_index: usize = 0;
    var f: usize = 0;
    while (f <= 28) : (f += 1) {
        var s: usize = f + 1;
        while (s <= 29) : (s += 1) {
            swaps[swap_index] = Swap{ .f = f + 1, .s = s + 1 };
            swap_index += 1;
        }
    }
    return swaps;
}

pub fn generateColSwaps() [45]Swap {
    var swaps = [1]Swap{undefined} ** 45;
    var swap_index: usize = 0;
    var f: usize = 0;
    while (f <= 8) : (f += 1) {
        var s: usize = f + 1;
        while (s <= 9) : (s += 1) {
            swaps[swap_index] = Swap{ .f = f, .s = s };
            swap_index += 1;
        }
    }
    return swaps;
}

pub fn swapColumns(a: usize, b: usize, layout: *[31]Bitmap) void {
    const am: usize = a + 1;
    const bm: usize = b + 1;
    swapIndices(indexOf(am, layout).?, indexOf(bm, layout).?, layout);
    swapIndices(indexOf(am + 10, layout).?, indexOf(bm + 10, layout).?, layout);
    swapIndices(indexOf(am + 20, layout).?, indexOf(bm + 20, layout).?, layout);
}

pub fn swapIndices(a: usize, b: usize, layout: *[31]Bitmap) void {
    const temp: Bitmap = layout[a];
    layout[a] = layout[b];
    layout[b] = temp;
}

pub fn initLayout(layout: *[31]Bitmap) void {
    const key: Bitmap = 0b1_00000_00000_00000_00000_00000_00000;
    var i: usize = 0;
    while (i < layout.len) : (i += 1) {
        layout[i] = key >> @intCast(u5, i);
    }
}

pub fn textToBitmaps(str: []const u8, symbols: []const u21, allocator: std.mem.Allocator) !*[31]Bitmap {
    var layout = try allocator.create([31]Bitmap);
    var sym_iter = std.mem.tokenize(u8, str, " \n");
    layout[0] = bitmap.indexToBitmap(0);
    var sym_index: usize = 1;
    while (sym_iter.next()) |text_symbol| {
        const cp = try std.unicode.utf8Decode(text_symbol);
        for (symbols) |table_symbol, index| {
            if (cp == table_symbol) layout[index] = bitmap.indexToBitmap(sym_index);
        }
        sym_index += 1;
    }
    return layout;
}

pub fn bitmapsToText(bitmaps: []Bitmap, symbols: []const u21, allocator: std.mem.Allocator) ![]const u8 {
    var bytes: usize = 0;
    for (symbols) |sym| {
        bytes += try std.unicode.utf8CodepointSequenceLength(sym);
        bytes += 1;
    }

    var str = try allocator.alloc(u8, bytes + 3 + 8);
    std.mem.set(u8, str, ' ');
    var head: usize = 0;
    var key: Bitmap = 0b10000_00000_00000_00000_00000_00000;
    var letters: usize = 0;
    while (key != 0) : (key >>= 1) {
        const index = std.mem.indexOf(Bitmap, bitmaps, &.{key}).?;
        if (index < symbols.len) {
            const cp = symbols[index];
            var buf: [4]u8 = undefined;
            const length = try std.unicode.utf8Encode(cp, buf[0..]);
            std.mem.copy(u8, str[head..], buf[0..length]);
            head += length;
        } else {
            head += 1;
        }
        letters += 1;

        if (letters == 10 or letters == 20 or letters == 30) str[head] = '\n';
        head += 1;
        if (letters == 5 or letters == 15 or letters == 25) {
            head += 1;
        }
    }
    return str;
}
