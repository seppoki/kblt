const std = @import("std");
const masks = @import("masks.zig");
const Bitmap = @import("bitmap.zig").Bitmap;

pub fn effort(a: Bitmap) usize {
    for (masks.efforts) |mask, index| {
        if (masks.inMask(a, mask)) return index;
    }
    unreachable;
}

pub fn isPinky(key: Bitmap) bool {
    return masks.inMask(key, masks.pinky);
}

pub fn isRing(key: Bitmap) bool {
    return masks.inMask(key, masks.ring);
}

pub fn isMiddle(key: Bitmap) bool {
    return masks.inMask(key, masks.middle);
}

pub fn isIndexClose(key: Bitmap) bool {
    return masks.inMask(key, masks.index_close);
}

pub fn isIndexStretch(key: Bitmap) bool {
    return masks.inMask(key, masks.index_stretch);
}

pub fn isThumb(key: Bitmap) bool {
    return masks.inMask(key, masks.thumb);
}

pub fn inOhPosition(a: Bitmap, b: Bitmap, c: Bitmap) bool {
    for (masks.oh_positions) |pos| {
        if (masks.allInMask(a | b | c, pos)) return true;
    }
    return false;
}

pub fn greatSfb(a: Bitmap, b: Bitmap) bool {
    for (masks.great_sfb) |pair| {
        if (masks.bothInMask(a, b, pair)) return true;
    }
    return false;
}

pub fn goodSfb(a: Bitmap, b: Bitmap) bool {
    for (masks.good_sfb) |pair| {
        if (masks.bothInMask(a, b, pair)) return true;
    }
    return false;
}

pub fn averageSfb(a: Bitmap, b: Bitmap) bool {
    for (masks.average_sfb) |pair| {
        if (masks.bothInMask(a, b, pair)) return true;
    }
    return false;
}

pub fn badSfb(a: Bitmap, b: Bitmap) bool {
    for (masks.bad_sfb) |pair| {
        if (masks.bothInMask(a, b, pair)) return true;
    }
    return false;
}

pub fn positionIndex(a: Bitmap, b: Bitmap) usize {
    for (masks.position_pairs) |pairs| {
        var i: usize = 0;
        while (i < 6) : (i += 1) {
            if (masks.inMask(a, pairs[0]) and masks.inMask(b, pairs[i + 1])) return i;
            if (masks.inMask(b, pairs[0]) and masks.inMask(a, pairs[i + 1])) return i;
        }
    }
    return 6;
}

pub fn rollIndex(a: Bitmap, b: Bitmap) ?usize {
    for (masks.roll_pairs) |pair, index| {
        if (masks.inMask(a, pair[0]) and masks.inMask(b, pair[1])) return index;
    }
    return null;
}

pub fn redirIndex(a: Bitmap, b: Bitmap, c: Bitmap) ?usize {
    for (masks.redirect_pairs) |pair, index| {
        if (masks.inMask(a, pair[0]) and masks.inMask(b, pair[1]) and masks.inMask(c, pair[2])) return index;
    }
    return null;
}

pub fn shRollIndex(a: Bitmap, b: Bitmap, c: Bitmap) ?usize {
    for (masks.onehand_pairs) |pair, index| {
        if (masks.inMask(a, pair[0]) and masks.inMask(b, pair[1]) and masks.inMask(c, pair[2])) return index;
    }
    return null;
}
