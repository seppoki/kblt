(declare-project
  :name "corpusgen"
  :description "Generates corpus file from text file.")

(declare-executable
  :name "corpusgen"
  :entry "main.janet")
