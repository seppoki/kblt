#!/usr/bin/env janet

(defn tokenize
  "Splits string while discarding empty substrings"
  [delim str]
  (filter (fn [x] (not (empty? x)))
    (string/split delim str)))

(defn count-words
  [path]
  (with [file (file/open path :r)]
    (var word-pairs @{})
    (loop
      [line :iterate (file/read file :line)]
      (do
        (def trimmed (string/trim line))
        (def tokens (tokenize " " trimmed))
        (var prev (tokens 0))
        (var i 1)
        (while (< i (length tokens))
          (do
            (def word-pair (string prev " " (tokens i)))
            (def count_ (get word-pairs word-pair))
            (if count_
              (put word-pairs word-pair (+ count_ 1))
              (put word-pairs word-pair 1))
            (set prev (tokens i))
            (++ i)))))
    word-pairs))

(defn generate-curated-array
  [tbl & filters]
  (do
    (var wc-pairs (pairs tbl))
    (set wc-pairs (filter (fn [(w c)] (>= c 10)) wc-pairs))
    (reverse
      (sort-by
        (fn [(w c)] c)
        wc-pairs))))

(defn write-counts
  [word-count-pairs path]
  (with [file (file/open path :w)]
    (each (w c) word-count-pairs
      (file/write file (string/format "%d %s\n" c w)))))

(defn main
  [& args]
  (def in-path (get args 1))
  (def out-path (get args 2))
  (def wc-table (count-words in-path))
  (def ca (generate-curated-array wc-table))
  (write-counts ca out-path)
  (pp args))
