const std = @import("std");

pub const Bitmap = u32;

pub fn indexToBitmap(position: usize) Bitmap {
    const first: u32 = 0b1_00000_00000_00000_00000_00000_00000;
    return first >> @intCast(u5, position);
}

pub fn bitmapToIndex(bitmap: Bitmap) u5 {
    return 30 - @intCast(u5, @ctz(bitmap));
}

