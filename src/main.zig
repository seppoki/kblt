const std = @import("std");
const model = @import("model.zig");
const bitmap = @import("bitmap.zig");
const Bitmap = bitmap.Bitmap;
const layout = @import("layout.zig");
const Swap = layout.Swap;
const masks = @import("masks.zig");
const analysis = @import("analysis.zig");
const evaluation = @import("evaluation.zig");

pub fn main() anyerror!void {
    const stdout = std.io.getStdOut().writer();
    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();
    var allocator = arena.allocator();

    var prng = std.rand.DefaultPrng.init(@bitCast(u64, std.time.milliTimestamp()));
    const random = prng.random();

    var it = try std.process.argsWithAllocator(allocator);
    _ = it.next() orelse unreachable;
    const path = it.next() orelse unreachable;
    const m = try model.getModel(path, allocator);

    const a_index = std.mem.indexOf(u21, m.symbols[0..], &[_]u21{try std.unicode.utf8Decode("a")}).?;
    const e_index = std.mem.indexOf(u21, m.symbols[0..], &[_]u21{try std.unicode.utf8Decode("e")}).?;
    const i_index = std.mem.indexOf(u21, m.symbols[0..], &[_]u21{try std.unicode.utf8Decode("i")}).?;
    const o_index = std.mem.indexOf(u21, m.symbols[0..], &[_]u21{try std.unicode.utf8Decode("o")}).?;
    const u_index = std.mem.indexOf(u21, m.symbols[0..], &[_]u21{try std.unicode.utf8Decode("u")}).?;
    // const n_index = std.mem.indexOf(u21, m.symbols[0..], &[_]u21{try std.unicode.utf8Decode("n")}).?;

    var swaps: [435]Swap = layout.generateSwaps();
    var dbl_swaps_a: [435]Swap = layout.generateSwaps();
    var dbl_swaps_b: [435]Swap = layout.generateSwaps();
    var col_swaps: [45]Swap = layout.generateColSwaps();

    // for (m.monograms) |mono, i| {
    //     try stdout.print("{d}:\t{d:.6}\t", .{ i, mono.freq });
    //     var buf: [4]u8 = [1]u8{undefined} ** 4;
    //     var bytes = try std.unicode.utf8Encode(m.symbols[mono.index], buf[0..]);
    //     try stdout.print("{s}\n", .{buf[0..bytes]});
    // }

    // for (m.bigrams) |bi, i| {
    //     try stdout.print("{d}:\t{d:.6}\t", .{ i, bi.freq });
    //     var buf: [4]u8 = [1]u8{undefined} ** 4;
    //     var bytes = try std.unicode.utf8Encode(m.symbols[bi.first_index], buf[0..]);
    //     try stdout.print("{s}", .{buf[0..bytes]});
    //     bytes = try std.unicode.utf8Encode(m.symbols[bi.second_index], buf[0..]);
    //     try stdout.print("{s}\n", .{buf[0..bytes]});
    // }

    // for (m.trigrams) |tri, i| {
    //     try stdout.print("{d}:\t{d:.6}\t", .{ i, tri.freq });
    //     var buf: [4]u8 = [1]u8{undefined} ** 4;
    //     var bytes = try std.unicode.utf8Encode(m.symbols[tri.first_index], buf[0..]);
    //     try stdout.print("{s}", .{buf[0..bytes]});
    //     bytes = try std.unicode.utf8Encode(m.symbols[tri.second_index], buf[0..]);
    //     try stdout.print("{s}", .{buf[0..bytes]});
    //     bytes = try std.unicode.utf8Encode(m.symbols[tri.third_index], buf[0..]);
    //     try stdout.print("{s}\n", .{buf[0..bytes]});
    // }

    var l_best: [31]Bitmap = [1]Bitmap{undefined} ** 31;
    layout.initLayout(&l_best);
    var l: [31]Bitmap = [1]Bitmap{undefined} ** 31;
    var min_print_unfitness: f32 = std.math.f32_max;

    while (true) {
        var min_unfitness: f32 = std.math.f32_max;
        var iter_till_best: usize = 0;
        var swap_index: usize = 0;
        var dbl_swap_a_index: usize = 0;
        var dbl_swap_b_index: usize = 0;
        var col_swap_index: usize = 0;
        random.shuffle(Swap, swaps[0..]);
        random.shuffle(Swap, dbl_swaps_a[0..]);
        random.shuffle(Swap, dbl_swaps_b[0..]);
        random.shuffle(Swap, col_swaps[0..]);
        random.shuffle(Bitmap, l_best[1..]);
        var iter_mono: u32 = 0;
        var iter_bi: u32 = 0;
        var iter_skip: u32 = 0;
        var iter_tri: u32 = 0;

        while (iter_till_best < swaps.len + col_swaps.len + dbl_swaps_a.len * dbl_swaps_b.len) {
            std.mem.copy(Bitmap, l[0..], l_best[0..]);

            if (iter_till_best < swaps.len) {
                const swap: Swap = swaps[swap_index];
                layout.swapIndices(swap.f, swap.s, &l);
                swap_index += 1;
                if (swap_index == swaps.len) swap_index = 0;
            } else if (iter_till_best < swaps.len + col_swaps.len) {
                const swap: Swap = col_swaps[col_swap_index];
                layout.swapColumns(swap.f, swap.s, &l);
                col_swap_index += 1;
                if (col_swap_index == col_swaps.len) col_swap_index = 0;
            } else {
                const swap_a: Swap = dbl_swaps_a[dbl_swap_a_index];
                const swap_b: Swap = dbl_swaps_b[dbl_swap_b_index];
                layout.swapIndices(swap_a.f, swap_a.s, &l);
                layout.swapIndices(swap_b.f, swap_b.s, &l);
                dbl_swap_b_index += 1;
                if (dbl_swap_b_index == dbl_swaps_b.len) {
                    dbl_swap_b_index = 0;
                    dbl_swap_a_index += 1;
                    if (dbl_swap_a_index == dbl_swaps_a.len) {
                        dbl_swap_a_index = 0;
                    }
                }
            }
            iter_till_best += 1;

            var a: analysis.Analysis = analysis.Analysis{};
            var u: f32 = 0.0;

            const a_map = l[a_index];
            const e_map = l[e_index];
            const i_map = l[i_index];
            const o_map = l[o_index];
            const u_map = l[u_index];
            // const n_map = l[n_index];

            // if (!masks.inMask(a_map, 0b00000_00110_00000_00111_00000_00000)) {
            //     u += 1000.0;
            // }
            // if (!masks.inMask(e_map, 0b00000_00110_00000_00111_00000_00000)) {
            //     u += 1000.0;
            // }
            // if (!masks.inMask(i_map, 0b00000_00110_00000_00111_00000_00000)) {
            //     u += 1000.0;
            // }
            // if (!masks.inMask(o_map, 0b00000_00110_00000_00111_00000_00000)) {
            //     u += 1000.0;
            // }
            // if (!masks.inMask(u_map, 0b00000_00110_00000_00111_00000_00000)) {
            //     u += 1000.0;
            // }

            // if (!masks.inMask(a_map, masks.right_hand)) {
            //     u += 1000.0;
            // }
            // if (!masks.inMask(e_map, masks.right_hand)) {
            //     u += 1000.0;
            // }
            // if (!masks.inMask(i_map, masks.right_hand)) {
            //     u += 1000.0;
            // }
            // if (!masks.inMask(o_map, masks.right_hand)) {
            //     u += 1000.0;
            // }
            // if (!masks.inMask(u_map, masks.right_hand)) {
            //     u += 1000.0;
            // }

            if (!masks.inMask(a_map, 0b00000_00000_00000_00100_00000_00000)) {
                u += 1000.0;
            }
            if (!masks.inMask(e_map, 0b00000_00000_00000_00010_00000_00000)) {
                u += 1000.0;
            }
            if (!masks.inMask(i_map, 0b00000_00000_00000_00001_00000_00000)) {
                u += 1000.0;
            }
            if (!masks.inMask(o_map, 0b00000_00100_00000_00000_00000_00000)) {
                u += 1000.0;
            }
            if (!masks.inMask(u_map, 0b00000_00010_00000_00000_00000_00000)) {
                u += 1000.0;
            }

            // if (masks.inMask(n_map, 0b00000_00000_00000_01000_00000_00000)) {
            //     u += 1000.0;
            // }

            if (u >= min_unfitness) continue;

            analysis.analyseMono(l, m, &a);
            u += evaluation.evaluateMono(a);
            iter_mono += 1;
            if (u >= min_unfitness) continue;

            analysis.analyseBigram(l, m, &a);
            u += evaluation.evaluateBigram(a);
            iter_bi += 1;
            if (u >= min_unfitness) continue;

            analysis.analyseSkipgram(l, m, &a);
            u += evaluation.evaluateSkipgram(a);
            iter_skip += 1;
            if (u >= min_unfitness) continue;

            analysis.analyseTrigram(l, m, &a);
            u += evaluation.evaluateTrigram(a);
            iter_tri += 1;
            if (u >= min_unfitness) continue;

            std.mem.copy(Bitmap, l_best[0..], l[0..]);
            iter_till_best = 0;
            min_unfitness = u;
        }

        if (min_unfitness < min_print_unfitness) {
            min_print_unfitness = min_unfitness;

            var a: analysis.Analysis = analysis.Analysis{};
            analysis.analyseMono(l_best, m, &a);
            analysis.analyseBigram(l_best, m, &a);
            analysis.analyseSkipgram(l_best, m, &a);
            analysis.analyseTrigram(l_best, m, &a);
            const ub = evaluation.evaluateMono(a) + evaluation.evaluateBigram(a) + evaluation.evaluateSkipgram(a) + evaluation.evaluateTrigram(a);
            try stdout.print("{d:.3} m:{d} b:{d} s:{d} t:{d}\n", .{ ub, iter_mono, iter_bi, iter_skip, iter_tri });
            try stdout.print("finger\t{d:.2} ({d:.2}) | {d:.2}\n", .{ a.l.p + a.l.r + a.l.m + a.l.ic + a.l.is, a.l.t, a.r.is + a.r.ic + a.r.m + a.r.r + a.r.p });
            try stdout.print("finger\t{d:.2} {d:.2} {d:.2} {d:.2} ({d:.2}) | ({d:.2}) {d:.2} {d:.2} {d:.2} {d:.2}\n", .{ a.l.p, a.l.r, a.l.m, a.l.ic, a.l.is, a.r.is, a.r.ic, a.r.m, a.r.r, a.r.p });
            try stdout.print("effort%\t{d:.2} {d:.2} {d:.2}\n", .{ a.l.effort[0] + a.r.effort[0], a.l.effort[0] + a.r.effort[0] + a.l.effort[1] + a.r.effort[1], a.l.effort[0] + a.r.effort[0] + a.l.effort[1] + a.r.effort[1] + a.l.effort[2] + a.r.effort[2] });
            try stdout.print("effort\t{d:.2} {d:.2} {d:.2} {d:.2} {d:.2} {d:.2} | {d:.2} {d:.2} {d:.2} {d:.2} {d:.2} {d:.2}\n", .{ a.l.effort[0], a.l.effort[1], a.l.effort[2], a.l.effort[3], a.l.effort[4], a.l.effort[5], a.r.effort[0], a.r.effort[1], a.r.effort[2], a.r.effort[3], a.r.effort[4], a.r.effort[5] });
            try stdout.print("roll\t{d:.3} <> {d:.3} \ttot {d:.3}\n", .{ a.l.ro, a.r.ro, a.l.ro + a.r.ro });
            try stdout.print("alt\t{d:.3} <> {d:.3} \ttot {d:.3}\n", .{ a.l.alt, a.r.alt, a.l.alt + a.r.alt });
            try stdout.print("sh\t{d:.3} <> {d:.3} \ttot {d:.3}\n", .{ a.l.oh, a.r.oh, a.l.oh + a.r.oh });
            try stdout.print("sh pos\t{d:.4} !{d:.4} <> {d:.4} !{d:.4}\n", .{ a.l.in_sh_position, a.l.non_sh_position, a.r.in_sh_position, a.r.non_sh_position });
            try stdout.print("pinky r\t{d:.4} <> {d:.4} ~ {d:.4} <> {d:.4}\n", .{ a.l.pinky_repeat, a.r.pinky_repeat, a.l.pinky_srepeat, a.r.pinky_srepeat });
            try stdout.print("great sfb\t{d:.5} <> {d:.5} ~ ({d:.5} : {d:.5}) <> ({d:.5} : {d:.5})\n", .{ a.l.great_sfb, a.r.great_sfb, a.l.great_sfs_sh, a.l.great_sfs_alt, a.r.great_sfs_sh, a.r.great_sfs_alt });
            try stdout.print("good sfb\t{d:.5} <> {d:.5} ~ ({d:.5} : {d:.5}) <> ({d:.5} : {d:.5})\n", .{ a.l.good_sfb, a.r.good_sfb, a.l.good_sfs_sh, a.l.good_sfs_alt, a.r.good_sfs_sh, a.r.good_sfs_alt });
            try stdout.print("average sfb\t{d:.5} <> {d:.5} ~ ({d:.5} : {d:.5}) <> ({d:.5} : {d:.5})\n", .{ a.l.average_sfb, a.r.average_sfb, a.l.average_sfs_sh, a.l.average_sfs_alt, a.r.average_sfs_sh, a.r.average_sfs_alt });
            try stdout.print("bad sfb\t\t{d:.5} <> {d:.5} ~ ({d:.5} : {d:.5}) <> ({d:.5} : {d:.5})\n", .{ a.l.bad_sfb, a.r.bad_sfb, a.l.bad_sfs_sh, a.l.bad_sfs_alt, a.r.bad_sfs_sh, a.r.bad_sfs_alt });
            try stdout.print("pair:\t\t{d:.3} {d:.3} {d:.3} {d:.3} {d:.3} {d:.3} !{d:.4} | {d:.3} {d:.3} {d:.3} {d:.3} {d:.3} {d:.3} !{d:.4}\n", .{ a.l.pair[0], a.l.pair[1], a.l.pair[2], a.l.pair[3], a.l.pair[4], a.l.pair[5], a.l.pair[6], a.r.pair[0], a.r.pair[1], a.r.pair[2], a.r.pair[3], a.r.pair[4], a.r.pair[5], a.r.pair[6] });
            try stdout.print("spair:\t\t{d:.3} {d:.3} {d:.3} {d:.3} {d:.3} {d:.3} !{d:.4} | {d:.3} {d:.3} {d:.3} {d:.3} {d:.3} {d:.3} !{d:.4}\n", .{ a.l.spair[0], a.l.spair[1], a.l.spair[2], a.l.spair[3], a.l.spair[4], a.l.spair[5], a.l.spair[6], a.r.spair[0], a.r.spair[1], a.r.spair[2], a.r.spair[3], a.r.spair[4], a.r.spair[5], a.r.spair[6] });

            try stdout.print("roll:\n", .{});
            for (a.l.roll) |r| {
                try stdout.print(" {d:.3}", .{ r });
            }
            try stdout.print("\n", .{});

            for (a.r.roll) |r| {
                try stdout.print(" {d:.3}", .{ r });
            }
            try stdout.print("\n", .{});

            try stdout.print("redirect:\n", .{});
            for (a.l.redir[27..]) |r| {
                try stdout.print(" {d:.3}", .{ r });
            }
            try stdout.print("\n", .{});

            for (a.r.redir[27..]) |r| {
                try stdout.print(" {d:.3}", .{ r });
            }
            try stdout.print("\n", .{});

            try stdout.print("onehand:\n", .{});
            for (a.l.shRoll[15..]) |r| {
                try stdout.print(" {d:.3}", .{ r });
            }
            try stdout.print("\n", .{});

            for (a.r.shRoll[15..]) |r| {
                try stdout.print(" {d:.3}", .{ r });
            }
            try stdout.print("\n", .{});


            const lstr = try layout.bitmapsToText(&l_best, m.symbols, allocator);
            try stdout.print("{s}\n", .{lstr});
        }
    }
}
