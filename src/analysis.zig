const std = @import("std");
const model = @import("model.zig");
const Model = model.Model;
const bitmap = @import("bitmap.zig");
const Bitmap = bitmap.Bitmap;
const metrics = @import("metrics.zig");
const masks = @import("masks.zig");

pub const Hand = struct {
    p: f32 = 0.0,
    r: f32 = 0.0,
    m: f32 = 0.0,
    ic: f32 = 0.0,
    is: f32 = 0.0,
    t: f32 = 0.0,
    effort: [6]f32 = [_]f32{0.0} ** 6,
    ro: f32 = 0.0,
    alt: f32 = 0.0,
    oh: f32 = 0.0,
    in_sh_position: f32 = 0.0,
    non_sh_position: f32 = 0.0,
    great_sfb: f32 = 0.0,
    good_sfb: f32 = 0.0,
    average_sfb: f32 = 0.0,
    bad_sfb: f32 = 0.0,
    pinky_repeat: f32 = 0.0,
    pinky_srepeat: f32 = 0.0,

    pair: [7]f32 = [_]f32{0.0} ** 7,
    roll: [20]f32 = [_]f32{0.0} ** 20,
    redir: [40]f32 = [_]f32{0.0} ** 40,
    shRoll: [20]f32 = [_]f32{0.0} ** 20,

    great_sfs: f32 = 0.0,
    good_sfs: f32 = 0.0,
    average_sfs: f32 = 0.0,
    bad_sfs: f32 = 0.0,

    great_sfs_alt: f32 = 0.0,
    good_sfs_alt: f32 = 0.0,
    average_sfs_alt: f32 = 0.0,
    bad_sfs_alt: f32 = 0.0,

    great_sfs_sh: f32 = 0.0,
    good_sfs_sh: f32 = 0.0,
    average_sfs_sh: f32 = 0.0,
    bad_sfs_sh: f32 = 0.0,

    spair: [7]f32 = [_]f32{0.0} ** 7,
    sroll: [20]f32 = [_]f32{0.0} ** 20,
};

pub const Analysis = struct {
    l: Hand = Hand{},
    r: Hand = Hand{},
};

pub fn analyseMono(l: [31]Bitmap, m: *Model, a: *Analysis) void {
    for (m.monograms) |mono| {
        const key = l[mono.index];
        const freq = mono.freq;
        const hand: *Hand = if (masks.inMask(key, masks.left_hand)) &a.l else &a.r;
        hand.effort[metrics.effort(key)] += freq;
        if (metrics.isPinky(key)) hand.p += freq;
        if (metrics.isRing(key)) hand.r += freq;
        if (metrics.isMiddle(key)) hand.m += freq;
        if (metrics.isIndexClose(key)) hand.ic += freq;
        if (metrics.isIndexStretch(key)) hand.is += freq;
        if (metrics.isThumb(key)) hand.t += freq;
    }
}

fn shBigram(f: Bitmap, s: Bitmap, freq: f32, hand: *Hand) void {
    if (f == s) {
        if (metrics.isPinky(f)) hand.pinky_repeat += freq;
        return;
    }
    if (metrics.greatSfb(f, s)) {
        hand.great_sfb += freq;
        return;
    }
    if (metrics.goodSfb(f, s)) {
        hand.good_sfb += freq;
        return;
    }
    if (metrics.averageSfb(f, s)) {
        hand.average_sfb += freq;
        return;
    }
    if (metrics.badSfb(f, s)) {
        hand.bad_sfb += freq;
        return;
    }
    hand.pair[metrics.positionIndex(f, s)] += freq;
    if (metrics.rollIndex(f, s)) |i| {
        hand.roll[i] += freq;
    }
}

pub fn analyseBigram(l: [31]Bitmap, m: *Model, a: *Analysis) void {
    for (m.bigrams) |bi| {
        const f = l[bi.first_index];
        const s = l[bi.second_index];
        const freq = bi.freq;
        if (masks.bothInMask(f, s, masks.left_hand)) {
            shBigram(f, s, freq, &a.l);
        } else if (masks.bothInMask(f, s, masks.right_hand)) {
            shBigram(f, s, freq, &a.r);
        }
    }
}

fn shSkipgram(f: Bitmap, s: Bitmap, freq: f32, hand: *Hand) void {
    if (f == s) {
        if (metrics.isPinky(f)) hand.pinky_srepeat += freq;
        return;
    }
    if (metrics.greatSfb(f, s)) {
        hand.great_sfs += freq;
        return;
    }
    if (metrics.goodSfb(f, s)) {
        hand.good_sfs += freq;
        return;
    }
    if (metrics.averageSfb(f, s)) {
        hand.average_sfs += freq;
        return;
    }
    if (metrics.badSfb(f, s)) {
        hand.bad_sfs += freq;
        return;
    }
    hand.spair[metrics.positionIndex(f, s)] += freq;
    if (metrics.rollIndex(f, s)) |i| {
        hand.sroll[i] += freq;
    }
}

pub fn analyseSkipgram(l: [31]Bitmap, m: *Model, a: *Analysis) void {
    for (m.skipgrams) |sg| {
        const f = l[sg.first_index];
        const s = l[sg.second_index];
        const freq = sg.freq;
        if (masks.bothInMask(f, s, masks.left_hand)) {
            shSkipgram(f, s, freq, &a.l);
        } else if (masks.bothInMask(f, s, masks.right_hand)) {
            shSkipgram(f, s, freq, &a.r);
        }
    }
}

fn onehand(f: Bitmap, s: Bitmap, t: Bitmap, freq: f32, h: *Hand) void {
    h.oh += freq;
    if (metrics.inOhPosition(f, s, t)) {
        h.in_sh_position += freq;
    } else {
        h.non_sh_position += freq;
    }
    if (f == t) return;
    if (metrics.greatSfb(f, t)) {
        h.great_sfs_sh += freq;
        return;
    }
    if (metrics.goodSfb(f, t)) {
        h.good_sfs_sh += freq;
        return;
    }
    if (metrics.averageSfb(f, t)) {
        h.average_sfs_sh += freq;
        return;
    }
    if (metrics.badSfb(f, t)) {
        h.bad_sfs_sh += freq;
        return;
    }
    if (f == t or f == s or s == t) return;
    if (metrics.redirIndex(f, s, t)) |i| {
        h.redir[i] += freq;
        return;
    }
    if (metrics.shRollIndex(f, s, t)) |i| {
        h.shRoll[i] += freq;
        return;
    }
}

fn alter(f: Bitmap, s: Bitmap, t: Bitmap, freq: f32, h: *Hand) void {
    _ = s;
    h.alt += freq;
    if (f == t) return;
    if (metrics.greatSfb(f, t)) {
        h.great_sfs_alt += freq;
        return;
    }
    if (metrics.goodSfb(f, t)) {
        h.good_sfs_alt += freq;
        return;
    }
    if (metrics.averageSfb(f, t)) {
        h.average_sfs_alt += freq;
        return;
    }
    if (metrics.badSfb(f, t)) {
        h.bad_sfs_alt += freq;
        return;
    }
}

fn roll(f: Bitmap, s: Bitmap, t: Bitmap, freq: f32, h: *Hand) void {
    _ = f;
    _ = s;
    _ = t;
    h.ro += freq;
}

pub fn analyseTrigram(l: [31]Bitmap, m: *Model, a: *Analysis) void {
    for (m.trigrams) |tri| {
        const f = l[tri.first_index];
        const s = l[tri.second_index];
        const t = l[tri.third_index];
        const freq = tri.freq;

        if (masks.bothInMask(f, t, masks.left_hand) and masks.inMask(s, masks.right_hand)) {
            alter(f, s, t, freq, &a.l);
        } else if (masks.bothInMask(f, t, masks.right_hand) and masks.inMask(s, masks.left_hand)) {
            alter(f, s, t, freq, &a.r);
        } else if (masks.allInMask(f | s | t, masks.left_hand)) {
            onehand(f, s, t, freq, &a.l);
        } else if (masks.allInMask(f | s | t, masks.right_hand)) {
            onehand(f, s, t, freq, &a.r);
        } else if (masks.bothInMask(f, s, masks.left_hand) and masks.inMask(t, masks.right_hand)) {
            roll(f, s, t, freq, &a.l);
        } else if (masks.bothInMask(s, t, masks.left_hand) and masks.inMask(f, masks.right_hand)) {
            roll(f, s, t, freq, &a.l);
        } else if (masks.bothInMask(f, s, masks.right_hand) and masks.inMask(t, masks.left_hand)) {
            roll(f, s, t, freq, &a.r);
        } else if (masks.bothInMask(s, t, masks.right_hand) and masks.inMask(f, masks.left_hand)) {
            roll(f, s, t, freq, &a.r);
        }
    }
}
